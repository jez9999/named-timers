// Type definitions for named-timers
// Definitions by: jez9999 <http://gooeysoftware.com/jsplugins/>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.5.3

export = named_timers;

declare function named_timers(timerName?: string): named_timers.NamedTimerCollection | named_timers.NamedTimer;

declare namespace named_timers {
	export interface NamedTimerCollection {
		createTimer(timerName: string, isOneOff?: boolean): NamedTimer;
		deleteTimer(timerName: string): void;
		getTimer(timerName: string): NamedTimer;
		getTimers(): { [timerName: string]: NamedTimer };
	}

	export interface NamedTimer {
		getTimerName(): string;
		getTimerType(): "oneOffTimer" | "repeatingTimer";
		startTimer(interval: number, callbackFn?: () => any, callImmediately?: boolean): NamedTimer;
		stopTimer(): NamedTimer;
		setTimerCallback(callbackFn?: () => any): NamedTimer;
	}
}
