This plugin allows for easier timer and callback management with named timer handles.

Note that you can use this plugin in a jQuery context, or a Node.js context with the following:

    const namedTimers = require("named-timers");

    namedTimers().createTimer("regularTimer").startTimer(5000,
        function() {
            // do some work...
            // ...
        }
    );

    namedTimers("regularTimer").stopTimer();

    namedTimers().createTimer("oneOffTimer", true).startTimer(5000,
        function() {
            // do some work...
            // ...
        }
    );

You can also just work with the NamedTimerCollection returned by the named-timers module export when called with no parameters:

    const namedTimers = require("named-timers")();

    namedTimers.getTimer("regularTimer").setTimerCallback(function() {
        // do different work...
        // ...
    });

If using Browserify, you can also extend an existing jQuery object with this plugin by passing it in instead of a timer name:

    var $ = require("jquery");
    require("named-timers")($);

    $('#someElement').namedTimers().createTimer("timer123").startTimer(...

For more information, please visit the plugin's homepage at:
http://gooeysoftware.com/jsplugins/named-timers/
