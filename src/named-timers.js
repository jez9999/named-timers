// Named timers
// by Jeremy Morton
// A JavaScript plugin that uses setTimeout/setInterval to allow for easier timer and callback management, with named timer handles. Works in a jQuery or a Node.js context.

// For a nice jQuery plugin development pattern, see:
// http://www.learningjquery.com/2007/10/a-plugin-development-pattern

(function($) {
	// *****
	// Private functions
	// *****

	function isPureInt(input) {
		return typeof input === "number" && /^[0-9]+$/.test(input);
	};

	function isFunction(input) {
		return typeof input === "function";
	};

	function isString(input, nonEmpty) {
		return typeof input === "string" && (!nonEmpty || input !== "");
	};

	function isJquery(input) {
		return (input && (
			(typeof jQuery !== "undefined" && (input instanceof jQuery)) ||
			(!isUnspecified(input.fn) && !isUnspecified(input.fn.jquery) && typeof(input.fn.jquery === "string"))
		));
	};

	function isUnspecified(input) {
		return input === null || input === undefined;
	};

	function getPureNumber(input) {
		return parseInt(input.replace(/[a-zA-Z]/g, ''));
	};

	// *****
	// Class definitions
	// *****

	// *** NamedTimerInternal class ***
	function NamedTimerInternal(timerName, isOneOff) {
		var self = this;

		//
		// Init class instance
		//

		self._timerName = timerName;
		self._timerId = null;
		self._callbackFn = null;
		self._currentContext = null;
		self._isOneOff = !!isOneOff;
	}

	//
	// Public methods
	//

	// Gets this timer's name.
	NamedTimerInternal.prototype.getTimerName = function() {
		var self = this;

		return self._timerName;
	};

	// Gets this timer's type.
	NamedTimerInternal.prototype.getTimerType = function() {
		var self = this;

		return self._isOneOff ? "oneOffTimer" : "repeatingTimer";
	};

	// Starts this named timer with the given interval.
	// interval: The interval to wait between each tick (in milliseconds).
	// callImmediately: (optional) If true, calls the callback function immediately instead of waiting for first tick.
	NamedTimerInternal.prototype.startTimer = function(interval, callImmediately) {
		var self = this;

		if (self._timerId !== null) {
			throw "Timer '" + self._timerName + "' is already started.";
		}

		if (isUnspecified(interval)) { throw "Timer interval must be specified."; }
		if (!isPureInt(interval)) { throw "Timer interval must be an integer."; }
		if (self._callbackFn === null) { throw "No timer callback function has been specified."; }

		// Start the timer
		if (self._isOneOff) { self._timerId = setTimeout(function() { self._timerId = null; self._callbackFn(); }, interval); }
		else { self._timerId = setInterval(function() { self._callbackFn(); }, interval); }
		if (callImmediately) { self._callbackFn(); }
	};

	// Stops this named timer.  If the timer isn't currently running, does nothing.
	NamedTimerInternal.prototype.stopTimer = function() {
		var self = this;

		if (self._timerId !== null) {
			if (self._isOneOff) { clearTimeout(self._timerId); }
			else { clearInterval(self._timerId); }
			self._timerId = null;
		}
	};

	// Sets the callback function for this timer.
	// callbackFn: The callback function for this timer.
	NamedTimerInternal.prototype.setTimerCallback = function(callbackFn) {
		var self = this;

		self._callbackFn = callbackFn;
	};

	// Sets the current context in which the callback function is operating.
	// jqCtx: The context.
	NamedTimerInternal.prototype.setCurrentContext = function(jqCtx) {
		var self = this;

		self._currentContext = jqCtx;
	};

	// Gets the current context in which the callback function is operating.
	NamedTimerInternal.prototype.getCurrentContext = function() {
		var self = this;

		return self._currentContext;
	};

	// *** NamedTimer class - holds jQuery context and communicates with NamedTimerInternal ***
	function NamedTimer(internalTimer, jqCtx) {
		var self = this;

		//
		// Init class instance
		//

		self._internalTimer = internalTimer;
		self._jqCtx = null;

		if (!isUnspecified(jqCtx)) { self._jqCtx = jqCtx; }
	}

	//
	// Public methods
	//

	// Gets this timer's name.
	NamedTimer.prototype.getTimerName = function() {
		var self = this;

		return self._internalTimer.getTimerName();
	};

	// Gets this timer's type.
	NamedTimer.prototype.getTimerType = function() {
		var self = this;

		return self._internalTimer.getTimerType();
	};

	// Starts this named timer with the given interval.
	// interval: The interval to wait between each tick (in milliseconds).
	// callbackFn: (optional) If set, sets the callback function to run on each tick.
	// callImmediately: (optional) If true, calls the callback function immediately instead of waiting for first tick.
	NamedTimer.prototype.startTimer = function(interval, callbackFn, callImmediately) {
		var self = this;

		if (!isUnspecified(callbackFn)) {
			// Set timer function with appropriate jQuery context
			self.setTimerCallback(callbackFn);
		}

		self._internalTimer.startTimer(interval, callImmediately);
		return self;
	};

	// Stops this named timer.  If the timer isn't currently running, does nothing.
	NamedTimer.prototype.stopTimer = function() {
		var self = this;

		self._internalTimer.stopTimer();
		return self;
	};

	// Sets the callback function for this timer.
	// callbackFn: The callback function for this timer.
	NamedTimer.prototype.setTimerCallback = function(callbackFn) {
		var self = this;

		if (isUnspecified(callbackFn)) { throw "Timer callback function must be specified."; }
		if (!isFunction(callbackFn)) { throw "Timer callback function must be a function."; }

		if (self._jqCtx === null) {
			self._jqCtx = self._internalTimer.getCurrentContext();
		}
		else {
			self._internalTimer.setCurrentContext(self._jqCtx);
		}
		self._internalTimer.setTimerCallback(function() {
			if (self._jqCtx === null) { callbackFn(); }
			else { callbackFn.apply(self._jqCtx) }
		});
		return self;
	};

	// *** NamedTimerCollection class - holds collection of timers along with jQuery context, and allows for creation/deletion of timers ***
	function NamedTimerCollection(timers, jqCtx) {
		var self = this;

		//
		// Init class instance
		//

		self._timers = timers;
		self._jqCtx = null;

		if (!isUnspecified(jqCtx)) { self._jqCtx = jqCtx; }
	}

	//
	// Public methods
	//

	// Create a new named timer.
	// timerName: The name of the new timer.
	// isOneOff: If true, creates a non-repeating timer that only fires once.
	NamedTimerCollection.prototype.createTimer = function(timerName, isOneOff) {
		var self = this;

		if (isUnspecified(timerName)) {
			throw "timerName must be specified.";
		}
		else if (timerName === "") {
			throw "timerName must not be an empty string.";
		}
		else if (timerName in self._timers) {
			throw "Timer '" + timerName + "' already exists.";
		}
		else {
			// Create internal timer with timerName and return its jQuery context wrapper
			return new NamedTimer(
				self._timers[timerName] = new NamedTimerInternal(timerName, isOneOff),
				self._jqCtx
			);
		}
	};

	NamedTimerCollection.prototype.deleteTimer = function(timerName) {
		var self = this;

		if (isUnspecified(timerName)) {
			throw "timerName must be specified.";
		}
		else if (timerName === "") {
			throw "timerName must not be an empty string.";
		}
		else if (!(timerName in self._timers)) {
			throw "Timer '" + timerName + "' doesn't exist.";
		}
		else {
			// Delete the specified timer, stopping it first if necessary
			self._timers[timerName].stopTimer();
			delete self._timers[timerName];
		}
	};

	NamedTimerCollection.prototype.getTimer = function(timerName) {
		var self = this;

		if (isUnspecified(timerName)) {
			throw "timerName must be specified.";
		}
		else if (timerName === "") {
			throw "timerName must not be an empty string.";
		}
		else if (!(timerName in self._timers)) {
			throw "Timer '" + timerName + "' doesn't exist.";
		}
		else {
			return new NamedTimer(self._timers[timerName], self._jqCtx);
		}
	};

	// Get a list of existing named timers.
	NamedTimerCollection.prototype.getTimers = function() {
		var self = this;

		// Returns object with a jQuery context-wrapped timer for each internal timer
		var wrappedTimers = {};
		for (var timerIntKey in self._timers) {
			wrappedTimers[self._timers[timerIntKey].getTimerName()] = new NamedTimer(self._timers[timerIntKey], self._jqCtx);
		}

		return wrappedTimers;
	};

	// *****
	// Private plugin members
	// *****

	var priv = {};

	// Object to hold internal named timers collection
	priv.timers = {};

	// *****
	// Public plugin definitions
	// *****

	function mainEntryPoint(timerName, jqCtx) {
		if (isJquery(timerName)) {
			// Looks like jQuery object; assume it is, and extend it with this plugin
			extendJquery(timerName);
		}
		else if (isUnspecified(timerName)) {
			// Nothing specified, return named timer collection
			return new NamedTimerCollection(priv.timers, jqCtx);
		}
		else if (!isString(timerName, true)) {
			// Timer name specified is not a non-empty string
			throw "timerName must be a non-empty string.";
		}
		else if (priv.timers[timerName] === undefined) {
			// Timer specified, does not exist
			throw "Timer '" + timerName + "' does not exist.";
		}
		else {
			// Existing timer specified, return it
			return new NamedTimer(priv.timers[timerName], jqCtx);
		}
	};

	function extendJquery(jqObject) {
		// Function to be called directly against jQuery, eg. jQuery.namedTimers()
		jqObject.namedTimers = mainEntryPoint;

		// Function that can be used with jQuery accessors, eg. jQuery('.myClass').namedTimers()
		jqObject.fn.namedTimers = function(timerName) {
			// $.namedTimers()         // --> NamedTimerCollection / NamedTimer
			//   .createTimer("foo")   // NamedTimerCollection
			//   .setTimerCallback(... // NamedTimer (jqCtx: null) -> NamedTimerInternal

			// $.namedTimers("foo")    // --> NamedTimerCollection / NamedTimer
			//   .setTimerCallback(... // NamedTimer (jqCtx: null) -> NamedTimerInternal

			// $('#divXyz')            // jQuery
			//   .namedTimers()        // --> NamedTimerCollection / NamedTimer
			//   .createTimer("foo")   // NamedTimerCollection
			//   .setTimerCallback(... // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal

			// var abc = $('#divXyz')  // jQuery
			//   .namedTimers()        // --> NamedTimerCollection / NamedTimer
			//   .createTimer("foo");  // NamedTimerCollection
			// abc                     // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal
			//   .setTimerCallback(... // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal

			// var abc = $('#divXyz')  // jQuery
			//   .namedTimers()        // --> NamedTimerCollection / NamedTimer
			//   ;                     // NamedTimerCollection

			// $('#divXyz')            // jQuery
			//   .namedTimers("foo")   // --> NamedTimerCollection / NamedTimer
			//   .setTimerCallback(... // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal

			// $('#divXyz')            // jQuery
			//   .namedTimers()        // --> NamedTimerCollection / NamedTimer
			//   .getTimers()          // Array[] of NamedTimer (jqCtx: [varied])
			//   [2]                   // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal
			//   .setTimerCallback(... // NamedTimer (jqCtx: '#divXyz') -> NamedTimerInternal

			return jqObject.namedTimers(timerName, this); // 'this', at this point in time, is jqCtx (jQuery context)
		};
	};

	// Register as AMD or in-browser jQuery
	if ($ === null) {
		module.exports = function(timerName) {
			return mainEntryPoint(timerName);
		};
	}
	else {
		extendJquery($);
	}
})(
	(function(){
		if (typeof jQuery !== "undefined") {
			return jQuery;
		}
		else if (typeof module === "object" && typeof module.exports === "object") {
			return null;
		}
		else {
			throw "No AMD export possible and jQuery isn't defined!  Unable to initialize.";
		}
	})()
);
